﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TreeForm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            MyTree<int, int> tree = new MyTree<int, int>();
            Random rnd = new Random();
            for (int i = 0; i < 100; ++i)
            {
                tree.Add(new KeyValuePair<int, int>(rnd.Next(100), rnd.Next(100)));
            }
            Graphics gr = e.Graphics;
            tree.ForEach(Test);
        }

        private static void Test(KeyValuePair<int, int> pair, int height, int width, bool RedBlack, bool left, bool right, int step)
        {
            if (step >= MainForm.ActiveForm.ClientSize.Width / 64)
            {
                string key = string.Empty;
                if (pair.Key < 10)
                {
                    key = string.Concat("0", pair.Key.ToString());
                }
                else
                {
                    key = pair.Key.ToString();
                }
                Graphics gr = ActiveForm.CreateGraphics();
                Pen pen = new Pen(Color.Red);
                Brush brush = Brushes.Red;
                Pen line = new Pen(Color.Blue);
                if (RedBlack)
                {
                    pen.Color = Color.Red;
                    brush = Brushes.Red;
                }
                else
                {
                    pen.Color = Color.Black;
                    brush = Brushes.Black;
                }
                gr.DrawEllipse(pen, new RectangleF(width, height, 20, 20));
                gr.DrawString(key, ActiveForm.Font, brush, width + 3, height + 5);
                if (step > MainForm.ActiveForm.ClientSize.Width / 64)
                {
                    if (left)
                    {
                        gr.DrawLine(line, new Point(width, height + 10), new Point(width - step / 2 + 10, height + MainForm.ActiveForm.ClientSize.Height / 10));
                    }
                    if (right)
                    {
                        gr.DrawLine(line, new Point(width + 20, height + 10), new Point(width + step / 2 + 10, height + MainForm.ActiveForm.ClientSize.Height / 10));
                    }
                }
            }

        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}
