﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeForm
{
    class MyTree<Tkey, TVal> where Tkey : IComparable
    {
        public delegate void ForEachDelegate(KeyValuePair<Tkey, TVal> pair, int height, int width, bool RedBlack, bool left, bool right, int step);

        private class TreeItem
        {
            public KeyValuePair<Tkey, TVal> _pair;
            public TreeItem _parent;
            public TreeItem _left;
            public TreeItem _right;
            public int _width, _height;
            public bool _redBlack = true;
            public bool _ifLeft = false;
            public bool _ifRight = false;
            public int _step = MainForm.ActiveForm.ClientSize.Width/2;
            public TreeItem(Tkey key, TVal value, TreeItem parent = null, TreeItem left = null, TreeItem right = null)
            {
                _pair = new KeyValuePair<Tkey, TVal>(key, value);
                _parent = parent;
                _left = left;
                _right = right;
            }
            public TreeItem(KeyValuePair<Tkey, TVal> pair, TreeItem parent = null, TreeItem left = null, TreeItem right = null)
            {
                _pair = pair;
                _parent = parent;
                _left = left;
                _right = right;
            }
        }

        private TreeItem _root = null;
        private int _counter = 0;
        private bool _allowDuplicateKeys;        
        

        public MyTree(bool allowDuplicateKeys = false)
        {
            _allowDuplicateKeys = allowDuplicateKeys;
        }

        public void Add(KeyValuePair<Tkey, TVal> pair)
        {
            Add(pair, _root);
        }
        private void Add(KeyValuePair<Tkey, TVal> pair, TreeItem item)
        {
            TreeItem newItem = new TreeItem(pair);
            ++_counter;
            if (item == null)
            {
                _root = newItem;
                _root._width = MainForm.ActiveForm.ClientSize.Width / 2-10;
                _root._height = MainForm.ActiveForm.ClientSize.Height / 6;
                return;
            }
            else
            {
                while (true)
                {
                    if (!_allowDuplicateKeys && pair.Key.CompareTo(item._pair.Key) == 0)
                    {
                        item._pair = pair;
                        return;
                    }
                    else if (pair.Key.CompareTo(item._pair.Key) < 0)
                    {
                        if (item._left == null)
                        {
                            item._left = new TreeItem(pair, item);
                            item._left._redBlack = !item._redBlack;
                            item._ifLeft = true;
                            item._left._step = item._step/2;
                            //item._left._width = item._width - 30;
                            item._left._width = (int)(item._width -item._left._step);
                            item._left._height = item._height + MainForm.ActiveForm.ClientSize.Height / 10; ;
                            ++_counter;
                            return;
                        }
                        else
                        {
                            item = item._left;
                        }
                    }
                    else
                    {
                        if (item._right == null)
                        {
                            item._right = new TreeItem(pair, item);
                            item._right._redBlack = !item._redBlack;
                            item._ifRight = true;
                            item._right._step = item._step/2;
                            item._right._width = (int)(item._width + item._right._step);
                           // item._right._width = item._width+30;
                            item._right._height = item._height  + MainForm.ActiveForm.ClientSize.Height / 10;
                            ++_counter;
                            return;
                        }
                        else
                        {
                            item = item._right;
                        }
                    }
                }
            }
        }
        public void ForEach(ForEachDelegate d)
        {
            if (_root != null)
            {
                ForEach(d, _root);
            }
        }
        private void ForEach(ForEachDelegate d, TreeItem item)
        {
            if (item._left != null)
            {
                ForEach(d, item._left);
            }

            d.Invoke(item._pair, item._height, item._width, item._redBlack, item._ifLeft, item._ifRight, item._step);

            if (item._right != null)
            {
                ForEach(d, item._right);
            }
        }
    }
}